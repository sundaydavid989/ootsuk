package `in`.co.android.turingo

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        //fVHH4Dz_vKo:APA91bFFaxnUbX1U5IaDjJR4Hy3ewEX8QSjsCXHDTVLXPGToK0HU0dnItAUqHsPDkLTn6jGoiq5LK4mqiCyWtvrM_xPn2GwgTHbuc5rInJ0f2Us2iUmqIIwSBH_Fg1czVDPBbhJsgrnc
        //token for sending to specific device
        //for sending to all device using own server code subscribe your app to one topic
        Log.d("TOken ", "" + FirebaseInstanceId.getInstance().token)
        FirebaseMessaging.getInstance().subscribeToTopic("allDevices")


        val PACKAGE_NAME = "com.android.chrome"

        val builder = CustomTabsIntent.Builder()

        builder.setToolbarColor(ContextCompat.getColor(this@MainActivity,R.color.colorPrimaryDark))
        builder.setSecondaryToolbarColor(ContextCompat.getColor(this@MainActivity,R.color.colorPrimary))
        builder.addDefaultShareMenuItem()
        builder.setShowTitle(true)

        val anotherCustomTab = builder.build()
        val intent = anotherCustomTab.intent
        intent.data = Uri.parse("https://www.ootsuk.com/home")

        val packageManager = packageManager
        val resolveInfoList = packageManager.queryIntentActivities(anotherCustomTab.intent,
            PackageManager.MATCH_DEFAULT_ONLY)

        for (resolveInfo in resolveInfoList) {
            val packageName = resolveInfo.activityInfo.packageName
            if (TextUtils.equals(packageName, PACKAGE_NAME))
                anotherCustomTab.intent.setPackage(PACKAGE_NAME)
        }
        anotherCustomTab.launchUrl(this, anotherCustomTab.intent.data)
    }
}
