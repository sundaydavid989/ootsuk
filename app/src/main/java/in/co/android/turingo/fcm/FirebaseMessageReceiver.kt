package `in`.co.android.turingo.fcm

import `in`.co.android.turingo.R
import `in`.co.android.turingo.SplashScreen
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


class FirebaseMessageReceiver : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        //handle when receive notification via data event
        if (remoteMessage.data.isNotEmpty()) {
            showNotification(
                remoteMessage.data["title"],
                remoteMessage.data["message"]
            )
        }

        //handle when receive notification
        if (remoteMessage.notification != null) {
            showNotification(
                remoteMessage.notification!!.title,
                remoteMessage.notification!!.body
            )
        }
    }

    private fun getCustomDesign(title: String, message: String): RemoteViews {
        val remoteViews =
            RemoteViews(applicationContext.packageName, R.layout.notification)
        remoteViews.setTextViewText(R.id.title, title)
        remoteViews.setTextViewText(R.id.message, message)
        remoteViews.setImageViewResource(R.id.icon, R.drawable.ic_launcherl_background)
        return remoteViews
    }

    fun showNotification(title: String?, message: String?) {
        val intent = Intent(this, SplashScreen::class.java)
        val channel_id = "web_app_channel"
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent =
            PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        val uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        var builder =
            NotificationCompat.Builder(applicationContext, channel_id)
                .setSmallIcon(R.drawable.ic_launcherl_background)
                .setSound(uri)
                .setAutoCancel(true)
                .setVibrate(longArrayOf(1000, 1000, 1000, 1000, 1000))
                .setOnlyAlertOnce(true)
                .setContentIntent(pendingIntent)
        builder =
            builder.setContent(getCustomDesign(title!!, message!!))
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel =
                NotificationChannel(channel_id, "web_app", NotificationManager.IMPORTANCE_HIGH)
            notificationChannel.setSound(uri, null)
            notificationManager.createNotificationChannel(notificationChannel)
        }
        notificationManager.notify(0, builder.build())
    } //app part ready now let see how to send differnet users
    //like send to specific device
    //like specifi topic
}
